FROM docker.io/jonathonf/manjaro:latest

MAINTAINER acrom18

RUN mkdir -p /root/.gnupg /root/.gnupg/dirmngr-cache.d
RUN touch /root/.gnupg/dirmngr_ldapservers.conf
RUN pacman -Sy
RUN gpg -k
RUN pacman-key --init
RUN pacman-key --populate archlinux
RUN pacman-key --populate manjaro 
RUN dirmngr < /dev/null
RUN pacman --quiet --noprogressbar -Su --noconfirm
RUN pacman --quiet --noprogressbar -S --noconfirm pacman-mirrors libusb ca-certificates

# cleanup
# https://unix.stackexchange.com/questions/52277/pacman-option-to-assume-yes-to-every-question#comment703795_52278
RUN yes | pacman --quiet --noprogressbar -Scc

