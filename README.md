# Docker Manjaro Base Image

A basic manjaro docker image. Based on docker.io/jonathonf/manjaro

## Get The Image

```shell
docker pull registry.gitlab.com/acrom18/manjaro-base
```
